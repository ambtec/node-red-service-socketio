import {
  NodeRedApp,
  EditorNodeProperties,
  EditorRED,
  NodeMessageInFlow,
} from "node-red";

export interface NodeRedAppCustom extends NodeRedApp {
  nodes: any;
}

export interface CustomEditorNodeProperties extends EditorNodeProperties {
  path: string;
  rules: CustomConfig[];
  room: string;
  socketid: string;
  sockettype: string;
  payloadkey: string;
}

export interface CustomConfig {
  v: string;
}
