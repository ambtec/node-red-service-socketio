// @ts-ignore
import Log from "ambtec-logger";
import {
  CustomEditorNodeProperties,
  NodeRedAppCustom,
  CustomConfig,
} from "../interfaces/red";
import {
  ResponseIamData,
  // @ts-ignore
} from "node-red-module-iam/dist/interfaces/iam";

import {
  CustomSocket,
  CustomSocketData,
  CustomSocketMsg,
  CustomSocketMsgSocket,
  SocketPayload,
} from "../interfaces/socket";

interface IamConfiguration {
  username: string;
  resource_access: string;
  groups: string[];
}

class SocketIo {
  prod: boolean;
  socketIo: any;
  callbacks: any;

  constructor(RED: NodeRedAppCustom, callbacks: any) {
    const { Server } = require("socket.io");
    const corsOrigin = process.env.SOCKETIO_CORS_ORIGINS
      ? process.env.SOCKETIO_CORS_ORIGINS.split(",")
      : [];
    const socketEndpoint = process.env.SOCKETIO_ENDPOINT
      ? process.env.SOCKETIO_ENDPOINT
      : "/socket.io";
    const corsHeaders = process.env.SOCKETIO_CORS_HEADERS
      ? process.env.SOCKETIO_CORS_HEADERS.split(",")
      : [];
    const corsCredentials = "true" === process.env.SOCKETIO_CORS_CREDENTIALS;

    this.prod = "true" === process.env.PRODUCTION;
    this.callbacks = callbacks;

    let ioOptions = {};
    if (corsOrigin.length) {
      ioOptions = {
        cors: {
          origin: corsOrigin,
          methods: ["GET", "POST"],
          allowedHeaders: corsHeaders,
          credentials: corsCredentials,
        },
      };
    }

    this.socketIo = new Server(RED.server, ioOptions);
    this.socketIo.serveClient(true);
    this.socketIo.path(socketEndpoint);
    this.socketIo.on("connection", this.handleConnect.bind(this));

    Log.debug({
      breadcrumbs: ["socket.io", "lib", "SocketIo"],
      message: "Init",
    });
  }

  protected handleConnect(socket: CustomSocket): void {
    const loginTimeout = process.env.SOCKETIO_LOGIN_TIMEOUT
      ? process.env.SOCKETIO_LOGIN_TIMEOUT
      : 2000;

    Log.debug({
      breadcrumbs: ["socket.io", socket.id, "connection"],
      message: "Socket connected",
    });

    if (this.callbacks.connect) this.callbacks.connect(socket);

    // Set rules
    for (const ioEvent of this.getEvents()) {
      this.addEventListener(socket, ioEvent);
    }

    socket.on("disconnect", this.handleDisconnect.bind(this, socket));
    socket.joinedRooms = [];
    socket.connectionTimeout = setTimeout(
      this.handleConnectTimeout.bind(this, socket),
      loginTimeout
    );

    // Request login code by client
    this.socketIo.sockets.sockets
      .get(socket.id)
      .emit("authenticate", "iam authentication is required");
  }

  protected handleConnectTimeout(socket: CustomSocket): void {
    if (this.callbacks.timeout) this.callbacks.timeout(socket);
  }

  protected handleDisconnect(socket: CustomSocket): void {
    if (this.callbacks.disconnect) this.callbacks.disconnect(socket);

    const localSocket =
      "string" === typeof socket ? this.getSocketById(socket) : socket;
    clearTimeout(localSocket.connectionTimeout);
    localSocket.disconnect();

    Log.debug({
      breadcrumbs: ["socket.io", socket.id, "handleDisconnect"],
      message: "disconnected",
    });
  }

  protected handleListenerEvent(
    socket: CustomSocket,
    eventId: string,
    data: any
  ) {
    if (this.callbacks.eventListener)
      this.callbacks.eventListener(socket, eventId, data);
  }

  protected addEventListener(socket: CustomSocket, eventId: string): void {
    if (!eventId.length) return;

    Log.debug({
      breadcrumbs: ["socket.io", socket.id, eventId],
      message: "Bind event listener",
    });

    socket.off(eventId, this.handleListenerEvent.bind(this, socket, eventId));
    socket.on(eventId, this.handleListenerEvent.bind(this, socket, eventId));
  }

  protected getEvents(): string[] {
    return process.env.SOCKETIO_EVENTS
      ? process.env.SOCKETIO_EVENTS.split(",")
      : [];
  }

  getSocketById(socketId: string): CustomSocket {
    return this.socketIo.sockets.sockets.get(socketId);
  }

  closeSocket(socket: CustomSocket | string): void {
    const localSocket =
      "string" === typeof socket ? this.getSocketById(socket) : socket;
    Log.debug({
      breadcrumbs: ["socket.io", localSocket.id, "closeSocket"],
    });
    this.handleDisconnect(localSocket);
  }

  emitToAudience(event: string, payload: any): void {
    this.socketIo.emit(event, payload);
  }

  emitToRoom(room: string, event: string, payload: any): void {
    this.socketIo.to(room).emit(event, payload);
  }

  joinRoom(socket: CustomSocket | string, room: string): void {
    const localSocket =
      "string" === typeof socket ? this.getSocketById(socket) : socket;
    localSocket.joinedRooms.push(room);
    localSocket.join(room);
  }

  hasRoom(socket: CustomSocket | string, room: string): boolean {
    const localSocket =
      "string" === typeof socket ? this.getSocketById(socket) : socket;
    return localSocket.joinedRooms.indexOf(room) > -1;
  }

  leaveRoom(socket: CustomSocket | string, room: string): void {
    const localSocket =
      "string" === typeof socket ? this.getSocketById(socket) : socket;
    localSocket.joinedRooms = localSocket.joinedRooms.splice(
      localSocket.joinedRooms.indexOf(room),
      1
    );
    localSocket.leave(room);
  }
}

export default SocketIo;
