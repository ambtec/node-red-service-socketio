import "./interfaces/node";
import {
  CustomEditorNodeProperties,
  NodeRedAppCustom,
  CustomConfig,
} from "./interfaces/red";
import {
  CustomSocket,
  CustomSocketMsg,
  CustomSocketMsgSocket,
  SocketPayload,
} from "./interfaces/socket";
// @ts-ignore
import Log from "ambtec-logger";
import SocketIo from "./libs/SocketIo";

module.exports = (RED: NodeRedAppCustom): void => {
  // const PROD = 'true' === process.env.PRODUCTION;
  const locals = new Map();
  const iamSocketMap = new Map();
  const garbageCollectionIntervalInSeconds = 1000 * 60 * 5; // 5 minutes
  const garbageCollectioConfigThresholdInSeconds = 1000 * 30; // 30 seconds
  const socketIo = new SocketIo(RED, {
    timeout(socket: CustomSocket): void {
      const sessionId = iamSocketMap.get(socket.id);
      Log.debug({
        breadcrumbs: ["socket.io", socket.id, "authenticate", "timeout"],
        message: "Check for timeout",
      });

      // If the socket isn't authenticate by now, disconnect
      if ("string" !== typeof sessionId) {
        Log.debug({
          breadcrumbs: ["socket.io", socket.id, "authenticate", "timeout"],
          message: "Timeout: Not logged, disconnecting socket",
        });
        socket.disconnect();
        return;
      }

      Log.debug({
        breadcrumbs: ["socket.io", socket.id, "authenticate", "timeout"],
        message: "Check for timeout passed",
      });
    },
    eventListener(socket: CustomSocket, eventId: string, data: any): void {
      if ("disconnect" === eventId) return;
      const parsedData = isJson(data) ? JSON.parse(data) : data;

      Log.debug({
        breadcrumbs: ["socket.io", socket.id, eventId, "eventListener"],
        message: `RAW(${data})`,
        properties: {
          parsedData: parsedData,
          event: eventId,
          id: socket.id,
          iam: socket.iam,
        },
      });

      if ("logout" === eventId) {
        socket.disconnect();
        return;
      }

      if ("authenticate" === eventId) {
        const authKey = parsedData.auth;
        // Set / Update locals storage
        if (null === iamSocketMap.get(authKey) && locals.has(authKey)) {
          iamSocketMap.set(authKey, socket.id);
          iamSocketMap.set(socket.id, authKey);
          socket.iam = locals.get(authKey);
          locals.delete(authKey);
        }
        return;
      }

      // If the socket isn't authenticated by now,
      // cancel processing and wait for authentication or timeout
      const sessionId = iamSocketMap.get(socket.id);
      if ("authenticate" !== eventId && !sessionId) {
        Log.warning({
          breadcrumbs: ["socket.io", socket.id, eventId],
          message: "Login required",
        });
        return;
      }

      const socketPayload: SocketPayload = {
        payload: parsedData,
        socket: {
          event: eventId,
          id: socket.id,
        },
        // iam: {
        //   // Rebuild IAM node for node flows (should be provieded by IAM event and bound to socket)
        //   // username: decodedDataValue.preferred_username,
        //   // resource_access: decodedDataValue.resource_access,
        //   // groups: decodedDataValue.groups
        //   username: socket.iam.username,
        //   resource_access: socket.iam.resource_access,
        //   groups: socket.iam.groups
        // }
        iam: socket.iam,
      };
      process.emit(eventId, socketPayload);
    },
    disconnect(socket: CustomSocket): void {
      const sessionId = iamSocketMap.get(socket.id);
      Log.debug({
        breadcrumbs: ["socket.io", socket.id, "disconnect"],
        properties: {
          socketId: socket.id,
          sessionId: sessionId,
        },
      });
      iamSocketMap.delete(socket.id);
      if (sessionId) iamSocketMap.delete(sessionId);
    },
  });

  /**
   * Garbage collector checkes locals and remove dead configurations to free up memory
   */
  const garbageCollector = (): void => {
    for (let [sessionId, config] of locals) {
      // If locals configuration is older then threshold
      if (
        config.creationTime <
        Date.now() - garbageCollectioConfigThresholdInSeconds
      ) {
        const socketId = iamSocketMap.get(sessionId);
        // Check if socketId is mapped. If this is not the case, config is dead and can be removed
        if (null === socketId) {
          locals.delete(sessionId);
          iamSocketMap.delete(sessionId);
        }
      }
    }
  };

  /**
   * JSON validator function
   *
   * @param str
   * @returns boolean
   */
  const isJson = (str: string): boolean => {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  };

  /**
   * Fetch socket id by msg property or, if set, by session iam map
   *
   * @param msg
   * @param sessionId
   * @returns string | null
   */
  const getSocketId = (
    msg: CustomSocketMsg,
    sessionId: string
  ): string | null => {
    if (msg.socket.id) {
      return msg.socket.id;
    } else if (sessionId && iamSocketMap.has(sessionId)) {
      return iamSocketMap.get(sessionId);
    }
    return null;
  };

  /**
   * Emit msg to corresponding receivers
   *
   * @param socketId
   * @param msg
   * @returns void
   */
  const emitMsg = (socketId: string, msg: CustomSocketMsg): void => {
    if (msg.socket.emit) {
      const socket = socketIo.getSocketById(socketId);

      switch (msg.socket.emit) {
        case "broadcast.emit":
          // Emit to all except client
          if (!socket) return;

          Log.debug({
            breadcrumbs: [
              "socket.io",
              socketId,
              msg.socket.event,
              msg.socket.emit,
            ],
            message: "broadcast.emit",
            properties: {
              msg: msg,
            },
          });
          // @ts-ignore
          socket.broadcast.emit(msg.socket.event, msg.payload);
          break;

        case "emit":
          // Emit to client only
          if (!socket) return;

          Log.debug({
            breadcrumbs: [
              "socket.io",
              socketId,
              msg.socket.event,
              msg.socket.emit,
            ],
            message: "emit",
            properties: {
              msg: msg,
            },
          });
          socket.emit(msg.socket.event, msg.payload);
          break;

        case "room":
          // Emit to room members
          if (!msg.payload.room) return;
          // if (!socket.rooms.has(msg.payload.room)) return;
          if (!socketIo.hasRoom(socket, msg.payload.room)) return;

          Log.debug({
            breadcrumbs: [
              "socket.io",
              socketId,
              msg.socket.event,
              msg.socket.emit,
            ],
            message: "room",
            properties: {
              msg: msg,
            },
          });
          socketIo.emitToRoom(msg.payload.room, msg.socket.event, msg.payload);

          break;

        default:
          // Emit to all
          Log.debug({
            breadcrumbs: [
              "socket.io",
              socketId,
              msg.socket.event,
              msg.socket.emit,
            ],
            message: "all",
            properties: {
              msg: msg,
            },
          });
          socketIo.emitToAudience(msg.socket.event, msg.payload);
      }
    }
  };

  /**
   * Handle emit event, run basic verifications before hand
   *
   * @param msg
   * @returns
   */
  const handleEmit = (msg: CustomSocketMsg): void => {
    const sessionId = msg.iam?.session ? msg.iam.session : null;

    if (
      msg.payload.room &&
      "room" === msg.socket.emit &&
      "SYSTEM" === msg.socket?.id
    ) {
      Log.debug({
        breadcrumbs: ["socket.io", "SYSTEM", msg.socket.event, msg.socket.emit],
        message: "SYSTEM event triggered",
        properties: {
          msg: msg,
        },
      });
      socketIo.emitToRoom(msg.payload.room, msg.socket.event, msg.payload);
      return;
    }

    // On login event make sure no other session id do already exist
    if (
      "login" === msg.socket.event &&
      sessionId &&
      iamSocketMap.has(sessionId)
    ) {
      // To prevent double login make sure
      // current session id matches stored one
      // otherwise close socket
      const mapedSocketId = iamSocketMap.get(sessionId);
      if (mapedSocketId && mapedSocketId !== msg.socket.id) {
        Log.warning({
          breadcrumbs: ["socket.io", msg.socket.id, "double login event"],
          message: "Prevent double login. Disconnect socket.",
          properties: {
            mapedSocketId: mapedSocketId,
            msg: msg,
          },
        });

        // Emit to new client only, request logout
        socketIo.closeSocket(msg.socket.id);

        return;
      }
    }

    const socketId = getSocketId(msg, sessionId);

    if (!socketId) {
      Log.warning({
        breadcrumbs: ["socket.io", socketId, msg.socket.event],
        message: "socketId is not set",
        properties: {
          msg: msg,
        },
      });
      return;
    }

    emitMsg(socketId, msg);

    // Handle disconnect event
    if (["logout", "disconnect"].indexOf(msg.socket.event) > -1) {
      Log.debug({
        breadcrumbs: ["socket.io", socketId, "disconnect"],
        message: "disconnected by event",
        properties: {
          msg: msg,
        },
      });
      socketIo.closeSocket(socketId);
    }
  };

  setInterval(garbageCollector, garbageCollectionIntervalInSeconds);

  // Node-Red events

  function socketIoReceive(config: CustomEditorNodeProperties): void {
    const node = this;
    RED.nodes.createNode(node, config);

    process.on(config.socketid, (msg: CustomSocketMsgSocket) => {
      node.send(msg);
    });
  }

  RED.nodes.registerType("socketio-receive", socketIoReceive);

  function socketIoReply(config: CustomEditorNodeProperties): void {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on("input", (msg: CustomSocketMsg): void => {
      const payloadKey = config.payloadkey ? config.payloadkey : null;
      if ("" !== config.sockettype) msg.socket.emit = config.sockettype;
      if ("" !== config.sockettype && payloadKey && msg[payloadKey])
        msg.payload = msg[payloadKey];
      if ("" !== config.sockettype && "" !== config.socketid)
        msg.socket.event = config.socketid;
      if ("room" === config.sockettype && "" !== config.room)
        msg.payload.room = config.room;
      handleEmit(msg);
    });
  }

  RED.nodes.registerType("socketio-reply", socketIoReply);

  function socketIoJoinRoom(config: CustomEditorNodeProperties): void {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on("input", (msg: CustomSocketMsg): void => {
      if ("room" === config.sockettype && "" !== config.room)
        msg.payload.room = config.room;
      if (!msg.payload.room) return;

      const socket = socketIo.getSocketById(msg.socket.id);
      if (!socket) return;

      // socket.rooms.set(msg.payload.room);
      // socket.join(msg.payload.room);
      socketIo.joinRoom(socket, msg.payload.room);

      Log.debug({
        breadcrumbs: ["socket.io", socket.id, msg.socket.event, "room"],
        message: "join room",
        properties: {
          msg: msg,
        },
      });

      node.send(msg);
    });
  }

  RED.nodes.registerType("socketio-join", socketIoJoinRoom);

  function socketIoLeaveRoom(config: CustomEditorNodeProperties): void {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on("input", (msg: CustomSocketMsg): void => {
      if ("room" === config.sockettype && "" !== config.room)
        msg.payload.room = config.room;
      if (!msg.payload.room) return;

      const socket = socketIo.getSocketById(msg.socket.id);
      if (!socket) return;

      // socket.rooms.delete(msg.payload.room);
      // socket.leave(msg.payload.room);
      socketIo.leaveRoom(socket, msg.payload.room);

      Log.debug({
        breadcrumbs: ["socket.io", socket.id, msg.socket.event, "room"],
        message: "leave room",
        properties: {
          msg: msg,
        },
      });

      node.send(msg);
    });
  }

  RED.nodes.registerType("socketio-leave", socketIoLeaveRoom);

  function socketIoRegisterUser(config: CustomEditorNodeProperties): void {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on("input", (msg: CustomSocketMsg): void => {
      // Register user to locals

      const sessionId = msg.payload?.sessionId;
      if (sessionId) {
        iamSocketMap.set(sessionId, null);
        locals.set(sessionId, {
          username: msg.payload?.details?.username,
          resource_access: [], // msg.payload?.details?.resource_access, // socket.iam.resource_access,
          groups: ["/DashboardEvents", "/SocketMsg", "/SocketRoomMsg"], // msg.payload?.details?.groups // socket.iam.groups,
          creationTime: new Date(),
        });
      }

      node.send(msg);
    });
  }

  RED.nodes.registerType("socketio-register", socketIoRegisterUser);

  function socketIoUnregisterUser(config: CustomEditorNodeProperties): void {
    const node = this;
    RED.nodes.createNode(node, config);

    node.on("input", (msg: CustomSocketMsg): void => {
      // Unregister user from locals

      const sessionId = msg.payload?.sessionId;
      if (sessionId) {
        const socketId = iamSocketMap.get(sessionId);
        iamSocketMap.delete(sessionId);
        locals.delete(sessionId);
        socketIo.closeSocket(socketId);
      }

      node.send(msg);
    });
  }

  RED.nodes.registerType("socketio-unregister", socketIoUnregisterUser);
};
