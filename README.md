<div align="center">
  <a href="https://ambtec.com" title="ambtec" target="_blank">
    <img src="./assets/ambtec-logo-gitlab.png" width="128" />
  </a>
  <p><i>create shape improve</i></p>
</div>

<hr />

# Node-Red Service SocketIO
Custom implementation of [Socket.IO](http://socket.io/) for [Node-RED](https://nodered.org/) as an alternative to the default Node-Red Websocket. Was done because the default websocket implementation do not offer important socket events (like client disconnect, reconnect and error).
## Installation
To install node-red-contrib-socketio use this command

`npm i https://gitlab.com/ambtec/node-red-module-socketio.git`

## Composition
The Socket.IO implementation prvoide:
* 1 node `` socketio-receive `` to listen to a `topic`
* 1 node `` socketio-reply `` to send msg.body by type `emit`, `broadcast.emit`, `room` or `all`. Can also be defined in `msg.payload.body`
* 1 node `` socketio-join `` to join a Socket IO room
* 1 node `` socketio-leave `` to leave a Socket IO room
* 1 node `` socketio-register `` to register a new user
* 1 node `` socketio-unregister `` to unregister a registered user

### ENV's
- `` IAM_ADDRESS_TOKEN `` : The IAM token endpoint `` /auth/realms/ambtec/protocol/openid-connect/token ``
- ``  IAM_ADDRESS_LOGOUT `` : The IAM logout endpoint `` /auth/realms/ambtec/protocol/openid-connect/logout ``
- `` SOCKETIO_CORS_ORIGINS `` Request origings to accept. Example: ``  http://localhost:8000,http://localhost:8080 ``
- `` SOCKETIO_CORS_HEADERS `` Additional CORS headers. Not in use yet.
- `` SOCKETIO_CORS_CREDENTIALS `` Accept CORS credentials. Example: `` true ``
- `` SOCKETIO_ENDPOINT `` Socket endpoint to listen at. Example: `` /socket.io ``
- `` SOCKETIO_EVENTS `` Socket events to listen at. New events must be registered here before they can be consumed in a flow. Example: `` authenticate,open,error,close,ping,packet,reconnect_attempt,reconnect,reconnect_error,reconnect_failed,connect,connect_error,disconnect,login,logout,testAll,testEmit,testBroadcast,joinRoom,sendRoom,leaveRoom,rooms,joinServiceEvent,leaveServiceEvent ``

### Registration and Authentication

Socket require a user authentication before a socket connection will be opened. There for a user must be registered by a `` socketio-register `` node with an IAM identification first. Then a user can connect and provide his IAM identification on `` authenticate `` event. If both id matches the socket connection is confirmed and established.

See [IMA Service](https://gitlab.com/ambtec/iam-service) to lean more about the login process.

Client Coummincation flow \
![Client Coummincation flow](./assets/Client-Communication.drawio.png "Client Coummincation flow") \
(Made with https://app.diagrams.net/)

## License
MIT

## Credits
* [@node-red-contrib-socketio](https://github.com/wperw/node-red-contrib-socketio) for inspiration